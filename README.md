According to the technical task, the product should be already in use in the next spring, and this should be MVP system. Since I am the only one who is going to work on the project, I am going to choose monolithic architecture. Why? Simple at the beginning, single deploy unit, simple codebase, resource efficient at small scale, simple to test. That's why I believe this will be enough at the very beginning of the project. Moreover, it seems to be easier to develop app with monolithic architecture if your dev team is small. There will be no issues with keeping everybody in loop.

Authorization can be implementing using passport.js.

Since PDF and invoice generation can be resource-intensive operations and require some time to be generated it will be more efficient to use third party service for this activity.

In order to alway operate the latest data regarding the rented/sold equipment + orders made at the bar, I suppose it will be good to use WebSockets to implement this kind of things. And maybe for processing payments. Everything else seems to be OK to use HTTP.

![Preview image](./a.jpg)
